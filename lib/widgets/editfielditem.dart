import 'package:flutter/material.dart';

class EditFieldItem extends StatelessWidget {
  final String title;
  final Widget field;

  EditFieldItem({this.title, this.field});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 27, right: 27, top: 20, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: TextStyle(color: Color.fromRGBO(93, 93, 93, 1))),
          field
        ],
      ),
    );
  }
}
