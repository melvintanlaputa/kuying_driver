import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SVGIcon extends StatelessWidget {
  final assetName;
  final color;
  final size;

  SVGIcon({this.assetName, this.color, this.size});

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      assetName,
      color: color,
      height: size,
      width: size,
    );
  }
}
