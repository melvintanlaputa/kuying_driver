import 'package:flutter/material.dart';

class TextFieldNoUnderline extends StatelessWidget {
  final TextEditingController controller;
  final bool enabled;

  TextFieldNoUnderline({this.controller, this.enabled});

  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: enabled,
      controller: controller,
      decoration: InputDecoration(
          hintText: '-',
          border: InputBorder.none,
          focusedBorder: InputBorder.none),
      style: TextStyle(
        color: enabled
            ? Color.fromRGBO(51, 51, 51, 1)
            : Color.fromRGBO(180, 180, 180, 1),
      ),
    );
  }
}
