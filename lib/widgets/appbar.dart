import 'package:flutter/material.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> actions;

  MyAppBar({Key key, this.title, this.actions})
      : preferredSize = Size.fromHeight(56.0),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  State createState() {
    return MyAppBarState();
  }
}

class MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      actions: widget.actions,
      title: Container(
          alignment: Alignment.center,
          child: Text(widget.title != null ? widget.title : '',
              style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1)))),
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
