import 'package:flutter/material.dart';
import 'package:kuying_driver/widgets/picker.dart';
import 'package:kuying_driver/widgets/editfielditem.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:kuying_driver/widgets/textfieldnounderline.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/classes/vehicle.dart';
import 'animatedListItem.dart';
import 'package:kuying_driver/classes/projector.dart';

class ProjectorWidget extends StatefulWidget {
  final int index;
  final List<Vehicle> vehicleList;
  final Projector projector;
  final Function deleteSelf;

  ProjectorWidget(
      {this.index, this.vehicleList, this.deleteSelf, this.projector});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProjectorWidgetState();
  }
}

class ProjectorWidgetState extends State<ProjectorWidget> {
  final vehicleController = TextEditingController();
  final projectorController = TextEditingController();

  var vehicleIndex = -1;

  @override
  Widget build(BuildContext context) {
    projectorController.text = widget.projector.name;

    List<Text> vehicleListText() {
      List<Text> textList = new List<Text>();
      widget.vehicleList.forEach((v) {
        textList.add(Text('${v.carMake} - ${v.carModel}'));
      });

      return textList;
    }

    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10, left: 20),
          child: Row(
            children: <Widget>[
              Text(
                'Projector ${widget.index}',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    if (widget.deleteSelf != null) {
                      widget.deleteSelf();
                    }
                  });
                },
                icon: SVGIcon(assetName: 'assets/svg/delete.svg'),
              )
            ],
          ),
        ),
        EditFieldItem(
          title: '模型 Model',
          field: TextFieldNoUnderline(
            controller: projectorController,
            enabled: false,
          ),
        ),
        CommonDivider(),
        EditFieldItem(
            title: '汽车模型 Choose which car this projector belongs to?',
            field: MyPicker(
                list: vehicleListText(),
                index: vehicleIndex,
                controller: vehicleController,
                callback: (int index) {
                  setState(() {
                    vehicleIndex = index;
                    vehicleController.text =
                        '${widget.vehicleList[vehicleIndex].carMake} - ${widget.vehicleList[vehicleIndex].carModel}';
                  });
                })),
        CommonDivider(),
        Container(height: 20)
      ],
    ));
  }
}
