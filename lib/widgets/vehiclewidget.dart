import 'package:flutter/material.dart';
import 'package:kuying_driver/widgets/picker.dart';
import 'package:kuying_driver/widgets/editfielditem.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:kuying_driver/widgets/textfieldnounderline.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/classes/vehicle.dart';
import 'animatedListItem.dart';
import 'package:kuying_driver/classes/projector.dart';

class VehicleWidget extends StatefulWidget {
  final int index;
  final List<Text> carMakeList;
  final List<List<Text>> carModelList;
  final Vehicle vehicle;
  final Function deleteSelf;

  VehicleWidget(
      {this.index,
      this.carMakeList,
      this.carModelList,
      this.vehicle,
      this.deleteSelf});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return VehicleWidgetState();
  }
}

class VehicleWidgetState extends State<VehicleWidget> {
  List<Projector> projectorList = [
    Projector(name: 'Epson 1100'),
    Projector(name: 'EPson 2000')
  ];

  final carMakeController = TextEditingController();
  final carModelController = TextEditingController();

  GlobalKey<AnimatedListState> _listKey = new GlobalKey<AnimatedListState>();

  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    List<Widget> widgetList = new List<Widget>();

    if (projectorList.length > 0) {
      if (index >= projectorList.length) {
        return null;
      }

      var p = projectorList[index];
      var _controller = TextEditingController();
      _controller.text = '没有联系 No projector linked';

      if (p != null) {
        _controller.text = p.name;
      }

      widgetList = [
        EditFieldItem(
          title: '放映机模型 ${index + 1} Projector Model ${index + 1}',
          field: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                  child: IgnorePointer(
                ignoring: true,
                child: TextFieldNoUnderline(
                  controller: _controller,
                  enabled: p != null ? true : false,
                ),
              )),
              p != null
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          projectorList.removeAt(index);
                          _listKey.currentState.removeItem(index,
                              (context, animation) {
                            return _buildRemovedItem(index, context, animation);
                          });
                        });
                      },
                      icon: SVGIcon(assetName: 'assets/svg/delete.svg'),
                    )
                  : IconButton(
                      onPressed: () {},
                      icon: SVGIcon(assetName: 'assets/svg/add_projector.svg'),
                    )
            ],
          ),
        ),
        CommonDivider()
      ];
    } else {
      widgetList = [
        Padding(
          padding: EdgeInsets.all(20),
          child: GestureDetector(
            onTap: () {
              var index = projectorList.length;
              projectorList.add(null);
              _listKey.currentState.insertItem(index);
            },
            child: Text(
              '+ Add more projectors',
              style: TextStyle(color: Theme.of(context).accentColor),
            ),
          ),
        )
      ];
    }

    if (index == projectorList.length - 1 && (projectorList[index] != null)) {
      widgetList.add(Padding(
        padding: EdgeInsets.all(20),
        child: GestureDetector(
          onTap: () {
            setState(() {
              var index = projectorList.length;
              projectorList.add(null);
              _listKey.currentState.insertItem(index);
            });
          },
          child: Text(
            '+ Add more projectors',
            style: TextStyle(color: Theme.of(context).accentColor),
          ),
        ),
      ));
    }

    return AnimatedListItem(
        animation: animation,
        item: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgetList,
        ));
  }

  Widget _buildRemovedItem(
      int index, BuildContext context, Animation<double> animation) {
    var _controller = TextEditingController();
    _controller.text = '-';

    return AnimatedListItem(
        animation: animation,
        item: Column(
          children: <Widget>[
            EditFieldItem(
              title: '放映机模型 ${index + 1} Projector Model ${index + 1}',
              field: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                      child: IgnorePointer(
                    ignoring: true,
                    child: TextFieldNoUnderline(
                      controller: _controller,
                      enabled: false,
                    ),
                  )),
                ],
              ),
            ),
            CommonDivider()
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    if (widget.vehicle.carMakeId == -1) {
      carMakeController.text = '-';
    } else {
      carMakeController.text =
          widget.carMakeList[widget.vehicle.carMakeId].data;
    }

    if (widget.vehicle.carModelId == -1) {
      carModelController.text = '-';
    } else {
      carModelController.text = widget
          .carModelList[widget.vehicle.carMakeId][widget.vehicle.carModelId]
          .data;
    }

    print('${widget.index}:${widget.vehicle.carMakeId}');
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10, left: 20),
          child: Row(
            children: <Widget>[
              Text(
                'Vehicle ${widget.index}',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    if (widget.deleteSelf != null) {
                      widget.deleteSelf();
                    }
                  });
                },
                icon: SVGIcon(assetName: 'assets/svg/delete.svg'),
              )
            ],
          ),
        ),
        EditFieldItem(
            title: '生产 Car Make',
            field: MyPicker(
                list: widget.carMakeList,
                index: widget.vehicle.carMakeId,
                controller: carMakeController,
                callback: (int index) {
                  setState(() {
                    widget.vehicle.carMakeId = index;
                    carMakeController.text =
                        widget.carMakeList[widget.vehicle.carMakeId].data;
                  });
                })),
        CommonDivider(),
        EditFieldItem(
            title: '汽车模型 Car Model',
            field: MyPicker(
                list: widget.vehicle.carMakeId >= 0
                    ? widget.carModelList[widget.vehicle.carMakeId]
                    : null,
                index: widget.vehicle.carModelId,
                controller: carModelController,
                callback: (int index) {
                  setState(() {
                    widget.vehicle.carModelId = index;
                    carModelController.text = widget
                        .carModelList[widget.vehicle.carMakeId]
                            [widget.vehicle.carModelId]
                        .data;
                  });
                })),
        CommonDivider(),
        AnimatedList(
          key: _listKey,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          initialItemCount: projectorList.length + 1,
          itemBuilder: _buildItem,
        )
      ],
    ));
  }
}
