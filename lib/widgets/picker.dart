import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kuying_driver/widgets/textfieldnounderline.dart';

class MyPicker extends StatefulWidget {
  final List<Text> list;
  final int index;
  final Function callback;
  final TextEditingController controller;

  MyPicker({this.list, this.index, this.callback, this.controller});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MyPickerState();
  }
}

class MyPickerState extends State<MyPicker> {
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: widget.list != null
            ? () {
                showModalBottomSheet(
                    context: context,
                    builder: (BuildContext builder) {
                      return Scaffold(
                          body: Container(
                        child: CupertinoPicker(
                            scrollController: FixedExtentScrollController(
                                initialItem: widget.index),
                            children: widget.list,
                            itemExtent: 50,
                            onSelectedItemChanged: widget.callback),
                      ));
                    });
              }
            : null,
        child: Container(
            color: Colors.transparent,
            child: IgnorePointer(
              ignoring: true,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: TextFieldNoUnderline(
                      controller: widget.controller,
                      enabled: widget.list != null ? true : false,
                    ),
                  ),
                  Icon(Icons.keyboard_arrow_down)
                ],
              ),
            )));
  }
}
