import 'package:flutter/material.dart';

class AnimatedListItem extends StatelessWidget {
  final Animation animation;
  final Widget item;

  const AnimatedListItem({Key key, this.animation, this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: animation,
      child: SizedBox(
        child: item,
      ),
    );
  }
}
