import 'package:flutter/material.dart';

class CommonDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Divider(
        color: Colors.grey.shade300,
        height: 5.0,
      ),
    );
  }
}
