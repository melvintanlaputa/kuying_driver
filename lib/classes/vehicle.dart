import 'package:kuying_driver/classes/projector.dart';

class Vehicle {
  List<Projector> projectorList = new List<Projector>();
  int carMakeId;
  int carModelId;
  String carMake;
  String carModel;

  Vehicle(
      {this.projectorList,
      this.carMake,
      this.carModel,
      this.carMakeId = -1,
      this.carModelId = -1});
}
