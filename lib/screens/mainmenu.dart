import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'dart:async';

class MainMenuScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MainMenuScreenState();
  }
}

class MainMenuScreenState extends State<MainMenuScreen>
    with TickerProviderStateMixin {
  var _phoneNum = '';
  var _verifyCode = '';
  var _seconds = 0;
  var _timer;
  var _verifyStr = '获取验证码';

  _startTimer() {
    _seconds = 10;

    _timer = new Timer.periodic(new Duration(seconds: 1), (timer) {
      if (_seconds == 0) {
        _cancelTimer();
        return;
      }

      _seconds--;
      _verifyStr = '$_seconds(s)';
      setState(() {});
      if (_seconds == 0) {
        _verifyStr = '重新发送';
      }
    });
  }

  _cancelTimer() {
    _timer?.cancel();
  }

  Widget _buildPhoneEdit() {
    var node = new FocusNode();
    return new Padding(
      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
      child: new TextField(
        onChanged: (str) {
          _phoneNum = str;
          setState(() {});
        },
        decoration: new InputDecoration(
          hintText: '请输入手机号',
          prefixIcon: SizedBox(
            child: Center(
              widthFactor: 0.0,
              child: Text('+86'),
            ),
          ),
        ),
        maxLines: 1,
        maxLength: 11,
        //键盘展示为号码
        keyboardType: TextInputType.phone,
        //只能输入数字
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
        ],
        onSubmitted: (text) {
          FocusScope.of(context).reparentIfNeeded(node);
        },
      ),
    );
  }

  Widget _buildVerifyCodeEdit() {
    var node = new FocusNode();
    Widget verifyCodeEdit = new TextField(
      onChanged: (str) {
        _verifyCode = str;
        setState(() {});
      },
      decoration: new InputDecoration(
        hintText: '请输入短信验证码',
      ),
      maxLines: 1,
      maxLength: 6,
      //键盘展示为数字
      keyboardType: TextInputType.number,
      //只能输入数字
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
      ],
      onSubmitted: (text) {
        FocusScope.of(context).reparentIfNeeded(node);
      },
    );

    Widget verifyCodeBtn = new InkWell(
      onTap: (_seconds == 0)
          ? () {
              setState(() {
                _startTimer();
              });
            }
          : null,
      child: new Container(
        alignment: Alignment.center,
        width: 100.0,
        height: 50.0,
        child: new Text(
          '$_verifyStr',
          style: new TextStyle(
              fontSize: 14.0, color: Color.fromRGBO(8, 121, 246, 1.0)),
        ),
      ),
    );

    return new Padding(
      padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
      child: new Stack(
        children: <Widget>[
          verifyCodeEdit,
          new Align(
            alignment: Alignment.topRight,
            child: verifyCodeBtn,
          ),
        ],
      ),
    );
  }

  Widget _buildRegister() {
    return new Padding(
      padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 20.0),
      child: new RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(40.0),
        ),
        color: Color.fromRGBO(8, 121, 246, 1.0),
        textColor: Colors.white,
        disabledColor: Color.fromRGBO(8, 121, 246, 0.4),
        onPressed: (_phoneNum.isEmpty || _verifyCode.isEmpty)
            ? null
            : () {
//                showTips();
                Navigator.of(context).pushNamed('/home');
              },
        child: new Text(
          "登  录",
          style: new TextStyle(fontSize: 16.0),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 100.0),
            child: Center(
              child: SVGIcon(
                assetName: 'assets/svg/ku_ying_logo.svg',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Center(
              child: SVGIcon(
                assetName: 'assets/svg/ku_ying_name.svg',
              ),
            ),
          ),
          Flexible(
              child: new ListView(children: [
            _buildPhoneEdit(),
            _buildVerifyCodeEdit(),
            _buildRegister(),
          ])),
        ],
      ),
    );
  }
}
