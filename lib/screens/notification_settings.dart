import 'package:flutter/material.dart';
import 'package:kuying_driver/widgets/appbar.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter/cupertino.dart';

class NotificationSettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new NotificationSettingsScreenState();
  }
}

class NotificationSettingsScreenState
    extends State<NotificationSettingsScreen> {
  var deviceSize;
  var versionNumber = '';
  SwitchWrapper push = SwitchWrapper(value: false);
  SwitchWrapper email = SwitchWrapper(value: false);
  SwitchWrapper promo = SwitchWrapper(value: false);

  Widget listItem(String title, SwitchWrapper s, VoidCallback callback) => Container(
        padding: EdgeInsets.only(left: 27, right: 27, top: 20, bottom: 20),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Text(title),
            ),
            CupertinoSwitch(
              value: s.value,
              onChanged: ((v) {
                setState(() {
                  print(v);
                  s.value = v;
                });
              }),
            )
          ],
        ),
      );

  Widget bodyData() {
    return Scaffold(
        appBar: MyAppBar(
          title: '设置 Settings',
        ),
        body: ListView(
          children: <Widget>[
            Text('Updates from 酷影'),
            Text('Enable push notifications to receive messages from 酷影.'),
            CommonDivider(),
            listItem('推送通知 Push notifications', push, () {}),
            CommonDivider(),
            listItem('电邮 Email', email, () {}),
            CommonDivider(),
            listItem('促销 Promotions', promo, () {}),
            CommonDivider(),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    PackageInfo.fromPlatform().then((packageInfo) {
      setState(() {
        versionNumber = packageInfo.version;
      });
    });
    return bodyData();
  }
}

class SwitchWrapper {
  bool value;

  SwitchWrapper({this.value});
}
