import 'package:flutter/material.dart';

class HomeTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HomeTabState();
  }
}

class HomeTabState extends State<HomeTab> {
  var campaign = new CampaignObject('侦探皮卡丘', 90, '广告详细信息', ['武昌', '汉阳']);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * .3,
            child: Center(
              child: Icon(Icons.ac_unit),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('标题 Title [${campaign.title}]'),
                SizedBox(height: 5),
                Text('运行 Runtime'),
                Text(Duration(seconds: campaign.duration).toString()),
                SizedBox(height: 5),
                Text('细节 Details'),
                Text(campaign.details),
                SizedBox(height: 5),
                Text('地点 Locations'),
                Text(
                    '${campaign.locations.toString().replaceAll("[", "").replaceAll("]", "")}'),
                SizedBox(height: 20),
                Center(
                  child: RaisedButton(
                      padding: EdgeInsets.all(10),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                      ),
                      elevation: 0,
                      color: Color.fromRGBO(255, 204, 107, 1),
                      onPressed: () {},
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.power,
                            color: Colors.white,
                          ),
                          Text(
                            '关闭',
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      )),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CampaignObject {
  String title;
  int duration;
  String details;
  List<String> locations;

  CampaignObject(
      String title, int duration, String details, List<String> locations) {
    this.title = title;
    this.duration = duration;
    this.details = details;
    this.locations = locations;
  }
}
