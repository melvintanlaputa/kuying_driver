import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/widgets/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:kuying_driver/widgets/vehiclewidget.dart';
import 'package:kuying_driver/classes/vehicle.dart';
import 'package:kuying_driver/widgets/animatedListItem.dart';

class VehicleScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new VehicleScreenState();
  }
}

class VehicleScreenState extends State<VehicleScreen> {
  var deviceSize;
  final GlobalKey<AnimatedListState> _listKey =
      new GlobalKey<AnimatedListState>();

  List<Vehicle> vehicleList = new List<Vehicle>();

  var carMakeList = [
    Text('Car Make A'),
    Text('Car Make B'),
    Text('Car Make C'),
    Text('Car Make D'),
  ];

  var carModelList = [
    [
      Text('Car Model AA'),
      Text('Car Model AB'),
    ],
    [
      Text('Car Model BA'),
      Text('Car Model BB'),
    ],
    [
      Text('Car Model CA'),
      Text('Car Model CB'),
    ],
    [
      Text('Car Model DA'),
      Text('Car Model DB'),
    ],
  ];

  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    if (index == 0) {
      return Align(
          alignment: Alignment.centerLeft,
          child: Padding(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 20),
              child: SVGIcon(
                assetName: 'assets/svg/vehicle.svg',
                color: Theme.of(context).accentColor,
                size: 40.0,
              )));
    }

    index -= 1;

    if (index >= vehicleList.length) {
      return Padding(
        padding: EdgeInsets.only(left: 20, bottom: 20),
        child: GestureDetector(
            onTap: () {
              setState(() {
                int index = vehicleList.length + 1;
                vehicleList.add(Vehicle());
                _listKey.currentState.insertItem(index);
              });
            },
            child: Row(
              children: <Widget>[
                SVGIcon(assetName: 'assets/svg/add.svg'),
                Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'Add more 车辆 Vehicles',
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold),
                    ))
              ],
            )),
      );
    }

    return AnimatedListItem(
      animation: animation,
      item: VehicleWidget(
        index: index + 1,
        vehicle: vehicleList[index],
        carMakeList: carMakeList,
        carModelList: carModelList,
        deleteSelf: () {
          setState(() {
            _listKey.currentState.removeItem(index + 1, (context, animation) {
              return _buildRemovedItem(index, context, animation);
            });
            vehicleList.removeAt(index);
          });
        },
      ),
    );
  }

  Widget _buildRemovedItem(
      int index, BuildContext context, Animation<double> animation) {
    return AnimatedListItem(
      animation: animation,
      item: VehicleWidget(
        index: index + 1,
        vehicle: Vehicle(),
        carMakeList: carMakeList,
        carModelList: carModelList,
        deleteSelf: () {},
      ),
    );
  }

  Widget bodyData() {
    return Scaffold(
      appBar: MyAppBar(title: '车辆 Vehicle', actions: [
        GestureDetector(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.only(right: 10),
            alignment: Alignment.center,
            child: Text('保存 Save',
                style: TextStyle(color: Theme.of(context).accentColor)),
          ),
        )
      ]),
      body: AnimatedList(
        key: _listKey,
        initialItemCount: vehicleList.length + 2,
        itemBuilder: _buildItem,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    return bodyData();
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
  }

  void getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
//      nameController.text = prefs.getString("name");
    });
  }

  void update() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.setString("name", nameController.text);

    Navigator.of(context).pop();
  }
}
