import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kuying_driver/widgets/editfielditem.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kuying_driver/widgets/appbar.dart';
import 'package:kuying_driver/widgets/textfieldnounderline.dart';
import 'package:flutter/cupertino.dart';
import 'package:kuying_driver/widgets/picker.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new EditProfileScreenState();
  }
}

class EditProfileScreenState extends State<EditProfileScreen> {
  var deviceSize;
  var _name = "-";

  var _image;
  var genderIndex = 0;
  var bankIndex = 0;

  final List<Text> genderList = [
    Text('-'),
    Text('男 Male'),
    Text('女 Female'),
    Text('别 Others'),
  ];

  final List<Text> bankList = [
    Text('-'),
    Text('Bank A'),
    Text('Bank B'),
    Text('Bank C'),
  ];

  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final dobController = TextEditingController();
  final genderController = TextEditingController();
  final emailController = TextEditingController();
  final bankController = TextEditingController();
  final carLicenseController = TextEditingController();
  final bankNoController = TextEditingController();

  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1900, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dobController.text = '${picked.day}.${picked.month}.${picked.year}';
      });
  }

  Widget profileColumn() => Container(
        padding: EdgeInsets.only(left: 20, right: 20),
        margin: EdgeInsets.only(bottom: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                flex: 4,
                child: Container(
                  child: GestureDetector(
                    onTap: () async {
                      await Navigator.of(context).pushNamed('/editProfile');
                      getSharedPrefs();
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _name,
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.w700),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                      ],
                    ),
                  ),
                )),
            Expanded(
                flex: 1,
                child: FittedBox(
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(40.0)),
                          border: new Border.all(
                            color: Color.fromRGBO(180, 180, 180, 1),
                            width: 2.0,
                          ),
                        ),
                        child: GestureDetector(
                          onTap: () async {
                            var image = await ImagePicker.pickImage(
                                source: ImageSource.camera);

                            setState(() {
                              _image = image;
                            });
                          },
                          child: CircleAvatar(
                            backgroundImage: _image != null
                                ? FileImage(_image)
                                : NetworkImage(
                                    "http://www.jk6.cc/api.php?url=http://mmbiz.qpic.cn/mmbiz_jpg/FzRaEgv5Ac1JGybk3VlclqVVpnu1TCxkVibkAJ4gbPhx1U5Urs9bc7BIEQ76MwtKBicNbJicgsPRKTGwxb9jCfroQ/0?wx_fmt=jpeg"),
                            foregroundColor: Colors.black,
                            radius: 30.0,
                          ),
                        ))))
          ],
        ),
      );

  Widget bodyData() {
    return Scaffold(
      appBar: MyAppBar(title: '细节 Profile', actions: [
        GestureDetector(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.only(right: 10),
            alignment: Alignment.center,
            child: Text('保存 Save',
                style: TextStyle(color: Theme.of(context).accentColor)),
          ),
        )
      ]),
      body: ListView(
        children: <Widget>[
          profileColumn(),
          CommonDivider(),
          EditFieldItem(
            title: '名称 Name',
            field: TextFieldNoUnderline(
              controller: nameController,
            ),
          ),
          CommonDivider(),
          EditFieldItem(
            title: '出生日期 Date of Birth',
            field: GestureDetector(
                onTap: () {
                  _selectDate(context);
                },
                child: Container(
                    color: Colors.transparent,
                    child: IgnorePointer(
                      ignoring: true,
                      child: TextFieldNoUnderline(
                        controller: dobController,
                      ),
                    ))),
          ),
          CommonDivider(),
          EditFieldItem(
              title: '性别 Gender',
              field: MyPicker(
                  list: genderList,
                  index: genderIndex,
                  controller: genderController,
                  callback: (int index) {
                    setState(() {
                      genderIndex = index;
                      genderController.text = genderList[index].data;
                    });
                  })),
          CommonDivider(),
          EditFieldItem(
            title: '电话 Phone No.',
            field: TextFieldNoUnderline(
              controller: phoneController,
            ),
          ),
          CommonDivider(),
          EditFieldItem(
            title: '汽车牌照 Car License Id',
            field: TextFieldNoUnderline(
              controller: carLicenseController,
            ),
          ),
          CommonDivider(),
          EditFieldItem(
              title: '银行 Bank',
              field: MyPicker(
                  list: bankList,
                  index: bankIndex,
                  controller: bankController,
                  callback: (int index) {
                    setState(() {
                      bankIndex = index;
                      bankController.text = bankList[index].data;
                    });
                  })),
          CommonDivider(),
          EditFieldItem(
            title: '银行账户 Bank Account No.',
            field: TextFieldNoUnderline(
              controller: bankNoController,
            ),
          ),
          CommonDivider(),
          EditFieldItem(
            title: '电子邮件 Email (optional)',
            field: TextFieldNoUnderline(
              controller: emailController,
            ),
          ),
          CommonDivider(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    return bodyData();
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
  }

  void getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      nameController.text = prefs.getString("name");
      phoneController.text = prefs.getString("phone");
      _name = prefs.getString("name");
    });
  }

  void update() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("name", nameController.text);
    prefs.setString("phone", phoneController.text);

    Navigator.of(context).pop();
  }
}
