import 'package:flutter/material.dart';

class MailboxTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MailboxTabState();
  }
}

class MailboxTabState extends State<MailboxTab> {
  List<Mail> mailList = [
    Mail('Welcome', 'Hi Welcome to blargh'),
    Mail('Verification', 'Please fill up your profile details'),
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      child: new Center(
          child: new RefreshIndicator(
        child: _buildMailList(context, mailList),
        onRefresh: _refreshList,
      )),
    );
  }

  ListView _buildMailList(context, List<Mail> mails) {
    return new ListView.builder(
      itemCount: mails.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text('${mails[index].title}'),
          subtitle: Text('${mails[index].body}'),
        );
      },
    );
  }

  Future<void> _refreshList() {
    setState(() {});
    return new Future.delayed(const Duration(seconds: 2), () {});
  }
}

class Mail {
  String title;
  String body;

  Mail(title, body) {
    this.title = title;
    this.body = body;
  }
}
