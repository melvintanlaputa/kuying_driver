import 'package:flutter/material.dart';
import 'package:kuying_driver/screens/profiletab.dart';
import 'package:kuying_driver/screens/hometab.dart';
import 'package:amap_base_location/amap_base_location.dart';
import 'performancetab.dart';
import 'mailboxtab.dart';
import 'package:kuying_driver/widgets/svgicon.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    print('home scren');
    // TODO: implement createState
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin {
  final _aMapLocation = AMapLocation();
  final _homeFile = 'assets/svg/home.svg';
  final _activityFile = 'assets/svg/activity.svg';
  final _notificationFile = 'assets/svg/notification.svg';
  final _profileFile = 'assets/svg/profile.svg';

  int _selectedIndex = 1;
  final _widgetOptions = [
    HomeTab(),
    ProfileTab(),
    PerformanceTab(),
    MailboxTab(),
    ProfileTab(),
  ];

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            customNavigationItem(_homeFile, '主页', 0),
            customNavigationItem(_activityFile, '活动', 1),
            middleNavigationItem(),
            customNavigationItem(_notificationFile, '邮箱', 3),
            customNavigationItem(_profileFile, '用户', 4),
          ],
        ),
      ),
    );
  }

  Widget customNavigationItem(String assetPath, String title, int index) {
    return Expanded(
      child: SizedBox(
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            onTap: () => _onItemTapped(index),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SVGIcon(
                    assetName: assetPath,
                    color: index == _selectedIndex
                        ? Color.fromRGBO(8, 121, 246, 1)
                        : Color.fromRGBO(180, 180, 180, 1)),
                Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: index == _selectedIndex
                          ? Color.fromRGBO(8, 121, 246, 1)
                          : Color.fromRGBO(180, 180, 180, 1)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget middleNavigationItem() {
    return _selectedIndex == 2
        ? RaisedButton(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
            ),
            color: Color.fromRGBO(8, 121, 246, 1),
            elevation: 0,
            onPressed: () {
              _onItemTapped(2);
            },
            child: Text(
              "性能",
              style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
            ))
        : OutlineButton(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
            ),
            borderSide: new BorderSide(color: Color.fromRGBO(8, 121, 246, 1)),
            onPressed: () {
              _onItemTapped(2);
            },
            child: Text(
              "性能",
              style: TextStyle(
                color: Color.fromRGBO(8, 121, 246, 1),
              ),
            ));
  }

  void _onItemTapped(int index) {
    print('ind: $index');
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
//    PermissionHandler().requestPermissions([PermissionGroup.location]).then(
//        _handlePermissionResult);
//    startLocationTracking();
  }

  @override
  void dispose() {
    _aMapLocation.stopLocate();
    super.dispose();
  }

  void startLocationTracking() async {
    final options = LocationClientOptions(
      isOnceLocation: false,
      locatingWithReGeocode: true,
    );

    if (await Permissions.requestMapPermission()) {
      _aMapLocation
          .startLocate(options)
          .map(_locationMap)
          .listen(_locationListener);
    }
  }

//  void _handlePermissionResult(Map<PermissionGroup, PermissionStatus> value) {
//    final locationPermission = value[PermissionGroup.location];
//
//    if (locationPermission == PermissionStatus.granted) {
//      amap.onLocationUpdated.listen(_locationUpdateHandler);
//    }
//  }
//

  void _locationMap(Location event) {
    print("map $event");
  }

  void _locationListener(void event) {}
}
