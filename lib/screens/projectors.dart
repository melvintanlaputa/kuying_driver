import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kuying_driver/widgets/appbar.dart';
import 'package:kuying_driver/widgets/textfieldnounderline.dart';
import 'package:flutter/cupertino.dart';
import 'package:kuying_driver/widgets/projectorwidget.dart';
import 'package:kuying_driver/classes/vehicle.dart';
import 'package:kuying_driver/classes/projector.dart';
import 'package:kuying_driver/widgets/animatedListItem.dart';

class ProjectorScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ProjectorScreenState();
  }
}

class ProjectorScreenState extends State<ProjectorScreen> {
  var deviceSize;
  final GlobalKey<AnimatedListState> _listKey =
      new GlobalKey<AnimatedListState>();

  List<Projector> projectorList = [];

  var vehicleList = [
    Vehicle(carMake: 'Haval', carModel: 'H1'),
    Vehicle(carMake: 'Haval', carModel: 'H2'),
    Vehicle(carMake: 'Haval', carModel: 'H3'),
  ];

  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    if (index == 0) {
      return Align(
          alignment: Alignment.centerLeft,
          child: Padding(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 20),
              child: SVGIcon(
                assetName: 'assets/svg/projector.svg',
                color: Theme.of(context).accentColor,
                size: 40.0,
              )));
    }

    index -= 1;

    if (index >= projectorList.length) {
      return Padding(
        padding: EdgeInsets.only(left: 20, bottom: 20),
        child: GestureDetector(
            onTap: () {
              setState(() {
                int index = projectorList.length + 1;
                projectorList.add(Projector(name: 'proj $index'));
                _listKey.currentState.insertItem(index);
              });
            },
            child: Row(
              children: <Widget>[
                SVGIcon(assetName: 'assets/svg/add_projector_withbg.svg'),
                Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'Add more 放映机 Projector',
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold),
                    ))
              ],
            )),
      );
    }

    return AnimatedListItem(
      animation: animation,
      item: ProjectorWidget(
        index: index + 1,
        projector: projectorList[index],
        vehicleList: vehicleList,
        deleteSelf: () {
          setState(() {
            _listKey.currentState.removeItem(index + 1, (context, animation) {
              return _buildRemovedItem(index, context, animation);
            });
            projectorList.removeAt(index);
          });
        },
      ),
    );
  }

  Widget _buildRemovedItem(
      int index, BuildContext context, Animation<double> animation) {
    return AnimatedListItem(
      animation: animation,
      item: ProjectorWidget(
        index: index + 1,
        projector: Projector(),
        vehicleList: vehicleList,
        deleteSelf: () {},
      ),
    );
  }

  Widget bodyData() {
    return Scaffold(
      appBar: MyAppBar(title: '放映机 Projector', actions: [
        GestureDetector(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.only(right: 10),
            alignment: Alignment.center,
            child: Text('保存 Save',
                style: TextStyle(color: Theme.of(context).accentColor)),
          ),
        )
      ]),
      body: AnimatedList(
        key: _listKey,
        initialItemCount: projectorList.length + 2,
        itemBuilder: _buildItem,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    return bodyData();
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
  }

  void getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
//      nameController.text = prefs.getString("name");
    });
  }

  void update() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.setString("name", nameController.text);

    Navigator.of(context).pop();
  }
}
