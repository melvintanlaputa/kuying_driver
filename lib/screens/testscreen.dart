import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'dart:async';

class TestScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new TestScreenState();
  }
}

class TestScreenState extends State<TestScreen> {
  final GlobalKey<AnimatedListState> _ListKey = GlobalKey();
  List<Color> listColors = [
    Colors.orange,
    Colors.green,
    Colors.red,
    Colors.blue,
    Colors.yellowAccent,
    Colors.brown,
  ];

  List<String> testList = ['asd', 'asd', 'asd', 'asd'];
  GlobalKey<AnimatedListState> _listKey = new GlobalKey<AnimatedListState>();

  SizeTransition _buildListItem(
    Color color,
    int index,
    Animation<double> animation,
  ) {
    return SizeTransition(
      sizeFactor: animation,
      child: List_Element(color, index),
    );
  }

  Widget List_Element(Color color, int index) {
    return Padding(
      padding: const EdgeInsets.all(4),
      child: Container(
        width: double.infinity,
        height: 50,
        child: RaisedButton(
          color: color,
          elevation: 2,
          child: Center(
            child: Text(
              "List item " + index.toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          onPressed: () {
            if (listColors.length > 0) {
              _removeFromList(index);
            }
          },
        ),
      ),
    );
  }

  void _removeFromList(int index) {
    var color = listColors[index + 1];
    _ListKey.currentState.removeItem(index + 1,
        (BuildContext context, Animation<double> animation) {
      return _buildListItem(color, index + 1, animation);
    });
    listColors.removeAt(index + 1);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: AnimatedList(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        key: _ListKey,
        initialItemCount: listColors.length,
        itemBuilder: (context, index, animation) {
          return _buildListItem(listColors[index], index, animation);
        },
      ),
    );
  }
}
