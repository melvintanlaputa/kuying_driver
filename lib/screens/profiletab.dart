import 'package:flutter/material.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

class ProfileTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ProfileTabState();
  }
}

class ProfileTabState extends State<ProfileTab> {
  var deviceSize;
  var _name;

  var _image;
  final _projectorFile = 'assets/svg/projector.svg';
  final _vehicleFile = 'assets/svg/vehicle.svg';
  final _settingsFile = 'assets/svg/settings.svg';
  final _logoutFile = 'assets/svg/logout.svg';

  Widget profileColumn() => Container(
        height: deviceSize.height * 0.24,
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                flex: 4,
                child: Container(
                  child: GestureDetector(
                    onTap: () async {
                      await Navigator.of(context).pushNamed('/editProfile');
                      getSharedPrefs();
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _name != null ? _name : '-',
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.w700),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          "查看编辑 View and Edit",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                )),
            Expanded(
                flex: 1,
                child: FittedBox(
                    child: Container(
                  decoration: BoxDecoration(
                    borderRadius:
                        new BorderRadius.all(new Radius.circular(40.0)),
                    border: new Border.all(
                      color: Color.fromRGBO(180, 180, 180, 1),
                      width: 2.0,
                    ),
                  ),
                  child: CircleAvatar(
                    backgroundImage: _image != null
                        ? FileImage(_image)
                        : NetworkImage(
                            "http://www.jk6.cc/api.php?url=http://mmbiz.qpic.cn/mmbiz_jpg/FzRaEgv5Ac1JGybk3VlclqVVpnu1TCxkVibkAJ4gbPhx1U5Urs9bc7BIEQ76MwtKBicNbJicgsPRKTGwxb9jCfroQ/0?wx_fmt=jpeg"),
                    foregroundColor: Colors.black,
                    radius: 30.0,
                  ),
                )))
          ],
        ),
      );

  Widget listItem(String title, String assetName, VoidCallback callback) =>
      GestureDetector(
          onTap: callback,
          child: Container(
            padding: EdgeInsets.only(left: 27, right: 27, top: 20, bottom: 20),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Text(title),
                ),
                Expanded(
                  flex: 1,
                  child: SVGIcon(
                    assetName: assetName,
                  ),
                )
              ],
            ),
          ));

  Widget bodyData() {
    return Scaffold(
      body: Column(
        children: <Widget>[
          profileColumn(),
          CommonDivider(),
          listItem('车辆 Vehicle', _vehicleFile, () async {
            await Navigator.of(context).pushNamed('/editVehicle');
          }),
          CommonDivider(),
          listItem('放映机 Projector', _projectorFile, () async {
            await Navigator.of(context).pushNamed('/editProjector');
          }),
          CommonDivider(),
          listItem('设置 Settings', _settingsFile, () async {
            await Navigator.of(context).pushNamed('/settings');
          }),
          CommonDivider(),
          listItem('登出 Logout', _logoutFile, () {}),
          CommonDivider(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    return bodyData();
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
  }

  void getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _name = prefs.getString("name");

    setState(() {});
  }
}
