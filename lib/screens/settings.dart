import 'package:flutter/material.dart';
import 'package:kuying_driver/widgets/appbar.dart';
import 'package:kuying_driver/widgets/svgicon.dart';
import 'package:kuying_driver/widgets/commondivider.dart';
import 'package:package_info/package_info.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new SettingsScreenState();
  }
}

class SettingsScreenState extends State<SettingsScreen> {
  var deviceSize;
  var versionNumber = '';

  Widget listItem(String title, VoidCallback callback) => GestureDetector(
      onTap: callback,
      child: Container(
        padding: EdgeInsets.only(left: 27, right: 27, top: 20, bottom: 20),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Text(title),
            ),
            Expanded(flex: 1, child: Icon(Icons.chevron_right))
          ],
        ),
      ));

  Widget bodyData() {
    return Scaffold(
        appBar: MyAppBar(
          title: '设置 Settings',
        ),
        body: ListView(
          children: <Widget>[
            Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                    padding: EdgeInsets.only(top: 20, left: 20, bottom: 20),
                    child: SVGIcon(
                      assetName: 'assets/svg/settings.svg',
                      color: Theme.of(context).accentColor,
                      size: 40.0,
                    ))),
            CommonDivider(),
            listItem('通知 Notification Settings', () async {
              await Navigator.of(context).pushNamed('/notificationSettings');
            }),
            CommonDivider(),
            listItem('服务条款 Terms of Service', () {}),
            CommonDivider(),
            listItem('隐私政策 Privacy Policy', () {}),
            CommonDivider(),
            Padding(
              padding:
                  EdgeInsets.only(left: 27, right: 27, top: 20, bottom: 20),
              child: Text('Version $versionNumber'),
            ),
            CommonDivider(),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    PackageInfo.fromPlatform().then((packageInfo) {
      setState(() {
        versionNumber = packageInfo.version;
      });
    });
    return bodyData();
  }
}
