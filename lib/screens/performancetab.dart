import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';

class PerformanceTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PerformanceTabState();
  }
}

class PerformanceTabState extends State<PerformanceTab> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new AnimatedCircularChart(
          size: Size(200, 200),
          initialChartData: <CircularStackEntry>[
            new CircularStackEntry(
              <CircularSegmentEntry>[
                new CircularSegmentEntry(
                  20,
                  Colors.blue[400],
                  rankKey: 'completed',
                ),
                new CircularSegmentEntry(
                  80,
                  Colors.blueGrey[600],
                  rankKey: 'remaining',
                ),
              ],
              rankKey: 'progress',
            ),
          ],
          holeLabel: '播放次数',
          chartType: CircularChartType.Radial,
          edgeStyle: SegmentEdgeStyle.round,
          percentageValues: true,
        ),
        new AnimatedCircularChart(
          size: Size(200, 200),
          initialChartData: <CircularStackEntry>[
            new CircularStackEntry(
              <CircularSegmentEntry>[
                new CircularSegmentEntry(
                  33.33,
                  Colors.blue[400],
                  rankKey: 'completed',
                ),
                new CircularSegmentEntry(
                  66.67,
                  Colors.blueGrey[600],
                  rankKey: 'remaining',
                ),
              ],
              rankKey: 'progress',
            ),
          ],
          holeLabel: '播放时间',
          chartType: CircularChartType.Radial,
          edgeStyle: SegmentEdgeStyle.round,
          percentageValues: true,
        ),
      ],
    );
  }
}
