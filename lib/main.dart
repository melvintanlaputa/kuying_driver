import 'package:flutter/material.dart';
import 'package:kuying_driver/screens/mainmenu.dart';
import 'package:kuying_driver/screens/home.dart';
import 'package:amap_base/amap_base.dart';
import 'package:kuying_driver/screens/edit_profile.dart';
import 'screens/vehicles.dart';
import 'screens/projectors.dart';
import 'screens/testscreen.dart';
import 'screens/settings.dart';
import 'screens/notification_settings.dart';

void main() async {
  await AMap.init('b63ace756bacae096002e2101c04b945');
  runApp(App());
}

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
              color: Colors.white,
              elevation: 0,
              iconTheme: IconThemeData(color: Colors.black)),
          primarySwatch: createMaterialColor(Color.fromRGBO(8, 121, 246, 1.0)),
          inputDecorationTheme: InputDecorationTheme(
              focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromRGBO(8, 121, 246, 1.0))))),
      home: MainMenuScreen(),
      routes: <String, WidgetBuilder>{
        '/mainmenu': (BuildContext context) => new MainMenuScreen(),
        '/home': (BuildContext context) => new HomeScreen(),
        '/editProfile': (BuildContext context) => new EditProfileScreen(),
        '/editVehicle': (BuildContext context) => new VehicleScreen(),
        '/editProjector': (BuildContext context) => new ProjectorScreen(),
        '/settings': (BuildContext context) => new SettingsScreen(),
        '/notificationSettings': (BuildContext context) => new NotificationSettingsScreen(),
        '/test/': (BuildContext context) => new TestScreen(),
      },
    );
  }

  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }
}
